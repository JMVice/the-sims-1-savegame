REM Builds full templates from the set of template data found on source safe...

ECHO Building SWEDISH templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\swedish\0_Sim_Lane"
mkdir "..\swedish\1_Sim_Lane"
mkdir "..\swedish\2_Sim_Lane"
mkdir "..\swedish\3_Sim_Lane"
mkdir "..\swedish\4_Sim_Lane"
mkdir "..\swedish\5_Sim_Lane"
mkdir "..\swedish\6_Sim_Lane"
mkdir "..\swedish\7_Sim_Lane"
mkdir "..\swedish\8_Sim_Lane"
mkdir "..\swedish\9_Sim_Lane"
mkdir "..\swedish\10_Sim_Lane"

attrib "..\swedish\0_Sim_Lane\*.*"  -r
attrib "..\swedish\1_Sim_Lane\*.*"  -r
attrib "..\swedish\2_Sim_Lane\*.*"  -r
attrib "..\swedish\3_Sim_Lane\*.*"  -r
attrib "..\swedish\4_Sim_Lane\*.*"  -r
attrib "..\swedish\5_Sim_Lane\*.*"  -r
attrib "..\swedish\6_Sim_Lane\*.*"  -r
attrib "..\swedish\7_Sim_Lane\*.*"  -r
attrib "..\swedish\8_Sim_Lane\*.*"  -r
attrib "..\swedish\9_Sim_Lane\*.*"  -r
attrib "..\swedish\10_Sim_Lane\*.*" -r
attrib "..\swedish\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\swedish\LotTemplates\*.*"   "..\swedish\0_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\1_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\2_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\3_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\4_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\5_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\6_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\7_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\8_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\9_Sim_Lane"
copy "..\swedish\LotTemplates\*.*"   "..\swedish\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\swedish\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\swedish\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\swedish\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\swedish\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\swedish\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\swedish\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\swedish\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\swedish\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\swedish\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\swedish\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\swedish\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\swedish\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\swedish\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\swedish\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\swedish\NeighborhoodGFX"
