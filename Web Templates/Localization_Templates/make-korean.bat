REM Builds full templates from the set of template data found on source safe...

ECHO Building KOREAN templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\korean\0_Sim_Lane"
mkdir "..\korean\1_Sim_Lane"
mkdir "..\korean\2_Sim_Lane"
mkdir "..\korean\3_Sim_Lane"
mkdir "..\korean\4_Sim_Lane"
mkdir "..\korean\5_Sim_Lane"
mkdir "..\korean\6_Sim_Lane"
mkdir "..\korean\7_Sim_Lane"
mkdir "..\korean\8_Sim_Lane"
mkdir "..\korean\9_Sim_Lane"
mkdir "..\korean\10_Sim_Lane"

attrib "..\korean\0_Sim_Lane\*.*"  -r
attrib "..\korean\1_Sim_Lane\*.*"  -r
attrib "..\korean\2_Sim_Lane\*.*"  -r
attrib "..\korean\3_Sim_Lane\*.*"  -r
attrib "..\korean\4_Sim_Lane\*.*"  -r
attrib "..\korean\5_Sim_Lane\*.*"  -r
attrib "..\korean\6_Sim_Lane\*.*"  -r
attrib "..\korean\7_Sim_Lane\*.*"  -r
attrib "..\korean\8_Sim_Lane\*.*"  -r
attrib "..\korean\9_Sim_Lane\*.*"  -r
attrib "..\korean\10_Sim_Lane\*.*" -r
attrib "..\korean\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\korean\LotTemplates\*.*"   "..\korean\0_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\1_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\2_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\3_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\4_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\5_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\6_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\7_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\8_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\9_Sim_Lane"
copy "..\korean\LotTemplates\*.*"   "..\korean\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\korean\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\korean\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\korean\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\korean\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\korean\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\korean\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\korean\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\korean\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\korean\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\korean\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\korean\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\korean\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\korean\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\korean\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\korean\NeighborhoodGFX"
