REM Builds full templates from the set of template data found on source safe...

ECHO Building SIMPLIFIED CHINESE templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\simplified chinese\0_Sim_Lane"
mkdir "..\simplified chinese\1_Sim_Lane"
mkdir "..\simplified chinese\2_Sim_Lane"
mkdir "..\simplified chinese\3_Sim_Lane"
mkdir "..\simplified chinese\4_Sim_Lane"
mkdir "..\simplified chinese\5_Sim_Lane"
mkdir "..\simplified chinese\6_Sim_Lane"
mkdir "..\simplified chinese\7_Sim_Lane"
mkdir "..\simplified chinese\8_Sim_Lane"
mkdir "..\simplified chinese\9_Sim_Lane"
mkdir "..\simplified chinese\10_Sim_Lane"

attrib "..\simplified chinese\0_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\1_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\2_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\3_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\4_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\5_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\6_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\7_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\8_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\9_Sim_Lane\*.*"  -r
attrib "..\simplified chinese\10_Sim_Lane\*.*" -r
attrib "..\simplified chinese\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\0_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\1_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\2_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\3_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\4_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\5_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\6_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\7_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\8_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\9_Sim_Lane"
copy "..\simplified chinese\LotTemplates\*.*"   "..\simplified chinese\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\simplified chinese\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\simplified chinese\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\simplified chinese\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\simplified chinese\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\simplified chinese\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\simplified chinese\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\simplified chinese\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\simplified chinese\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\simplified chinese\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\simplified chinese\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\simplified chinese\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\simplified chinese\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\simplified chinese\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\simplified chinese\NeighborhoodGFX"
