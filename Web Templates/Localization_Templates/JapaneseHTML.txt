token=####1####
english=####The ^^^^sims_family_name^^^^ Family Home Page####
foreign=#### ^^^^sims_family_name^^^^ 家族ホームページ####
desc=for example "The Smith Family Home Page"
--------------------------------------------------------
token=####2####
english=####Family Home Page####
foreign=####家族のホームページ####
desc=
--------------------------------------------------------
token=####3####
english=####Address:####
foreign=####アドレス：####
desc=
--------------------------------------------------------
token=####4####
english=####^^^^sims_house_address^^^^ Sim Lane####
foreign=####^^^^sims_house_address^^^^ シムレーン####
desc=
--------------------------------------------------------
token=####5####
english=####Cash Balance:####
foreign=####お金のバランス：####
desc=amount of spending money your family has
--------------------------------------------------------
token=####6####
english=####Days in Existence:####
foreign=####生活した日：####
desc=number of days your family has lived in this house
--------------------------------------------------------
token=####7####
english=####Number of Family Members:####
foreign=####家族メンバーの数：####
desc=
--------------------------------------------------------
token=####8####
english=####Number of Family Friends:####
foreign=####家族の友達の数：####
desc=
--------------------------------------------------------
token=####9####
english=####See the house.####
foreign=####家を見る####
desc=
--------------------------------------------------------
token=####10####
english=####This family is homeless.####
foreign=####家族はホームレスです####
desc=
--------------------------------------------------------
token=####11####
english=####See the neighborhood.####
foreign=####近所を見る####
desc=
--------------------------------------------------------
token=####12####
english=####House####
foreign=####家####
desc=
--------------------------------------------------------
token=####1001####
english=####Number ^^^^sims_house_address^^^^ Sim Lane:####
foreign=####^^^^sims_house_address^^^^ 番シムレーン：####
desc=
--------------------------------------------------------
token=####1002####
english=####Sim Lane:####
foreign=####シムレーン：####
desc=
--------------------------------------------------------
token=####1003####
english=####Address:####
foreign=####アドレス：####
desc=house address
--------------------------------------------------------
token=####1004####
english=####The ^^^^sims_family_name^^^^ Family Home####
foreign=#### ^^^^sims_family_name^^^^ 家族の家####
desc=As in "The Smith Family Home"
--------------------------------------------------------
token=####1005####
english=####Family Home####
foreign=####家族の家####
desc=
--------------------------------------------------------
token=####1006####
english=####Information####
foreign=####情報####
desc=
--------------------------------------------------------
token=####1007####
english=####Value:####
foreign=####価値：####
desc=cash value of the house
--------------------------------------------------------
token=####1008####
english=####Square Feet:####
foreign=####平方メートル：####
desc=floor space of the house measured in US feet
--------------------------------------------------------
token=####1009####
english=####Number of Floors:####
foreign=####フロア数：####
desc=number of levels/stories in the house
--------------------------------------------------------
token=####1010####
english=####Number of Bathrooms:####
foreign=####バスルームの数：####
desc=
--------------------------------------------------------
token=####1011####
english=####Number of Bedrooms:####
foreign=####寝室の数：####
desc=
--------------------------------------------------------
token=####1012####
english=####Statistics####
foreign=####統計：####
desc=
--------------------------------------------------------
token=####1013####
english=####Size:####
foreign=####サイズ：####
desc=
--------------------------------------------------------
token=####1014####
english=####Furnishings:####
foreign=####家財：####
desc=
--------------------------------------------------------
token=####1015####
english=####Yard:####
foreign=####庭：####
desc=
--------------------------------------------------------
token=####1016####
english=####Upkeep:####
foreign=####維持：####
desc=
--------------------------------------------------------
token=####1017####
english=####Layout:####
foreign=####設計：####
desc=
--------------------------------------------------------
token=####1018####
english=####Exterior####
foreign=####外観####
desc=
--------------------------------------------------------
token=####1019####
english=####First Floor Interior####
foreign=####1階のインテリア####
desc=
--------------------------------------------------------
token=####1020####
english=####Second Floor Interior####
foreign=####2階のインテリア####
desc=
--------------------------------------------------------
token=####2001####
english=####Bio####
foreign=####バイオグラフィー####
desc=short for biography
--------------------------------------------------------
token=####2002####
english=####Biography####
foreign=####バイオグラフィー####
desc=
--------------------------------------------------------
token=####2003####
english=####Page ^^^^sims_scrapbook_currpagenum^^^^ of ^^^^sims_scrapbook_numpages^^^^####
foreign=####^^^^sims_scrapbook_numpages^^^^のページ^^^^sims_scrapbook_currpagenum^^^^####
desc=shows which page your on out of the total pages in the address book
--------------------------------------------------------
token=####2004####
english=####Family####
foreign=####家族####
desc=
--------------------------------------------------------
token=####2005####
english=####Family Member####
foreign=####家族メンバー####
desc=
--------------------------------------------------------
token=####2006####
english=####The ^^^^sims_family_name^^^^ Family Home####
foreign=####^^^^sims_family_name^^^^ 家族の家####
desc=
--------------------------------------------------------
token=####2007####
english=####Family:####
foreign=####家族：####
desc=
--------------------------------------------------------
token=####2008####
english=####Gender:####
foreign=####性別：####
desc=
--------------------------------------------------------
token=####2009####
english=####Kid/Adult:####
foreign=####子供/大人：####
desc=is this person a kid or an adult
--------------------------------------------------------
token=####2010####
english=####Astrological Sign:####
foreign=####星座：####
desc=english zodiac sign like Leo, Libra, Aquarious, etc
--------------------------------------------------------
token=####2011####
english=####Career Path:####
foreign=####職業の道：####
desc=type of career like medical, or military
--------------------------------------------------------
token=####2012####
english=####Current Job:####
foreign=####現在の仕事：####
desc=
--------------------------------------------------------
token=####2013####
english=####Job Performance:####
foreign=####仕事のパフォーマンス：####
desc=
--------------------------------------------------------
token=####2014####
english=####Current Salary:####
foreign=####現在の給料：####
desc=
--------------------------------------------------------
token=####2015####
english=####Neat:####
foreign=####きれいさ：####
desc=how neat is this person
--------------------------------------------------------
token=####2016####
english=####Outgoing:####
foreign=####社交的：####
desc=how outgoing is this person
--------------------------------------------------------
token=####2017####
english=####Active:####
foreign=####活動的：####
desc=how active is this person
--------------------------------------------------------
token=####2018####
english=####Playful:####
foreign=####陽気：####
desc=how playful is this person
--------------------------------------------------------
token=####2019####
english=####Nice:####
foreign=####やさしさ：####
desc=how nice is this person
--------------------------------------------------------
token=####2020####
english=####Cooking:####
foreign=####料理：####
desc=how good at cooking is this person
--------------------------------------------------------
token=####2021####
english=####Mechanical:####
foreign=####技術：####
desc=how good at repairing machines is this person
--------------------------------------------------------
token=####2022####
english=####Charisma:####
foreign=####カリスマ：####
desc=how charismatic is this person
--------------------------------------------------------
token=####2023####
english=####Body:####
foreign=####身体：####
desc=how physically fit is this person
--------------------------------------------------------
token=####2024####
english=####Logic:####
foreign=####論理：####
desc=how logical is this person
--------------------------------------------------------
token=####2025####
english=####Creativity:####
foreign=####創作力：####
desc=how creative is this person
--------------------------------------------------------
token=####2026####
english=####Personality####
foreign=####人格####
desc=
--------------------------------------------------------
token=####2027####
english=####Skills####
foreign=####スキル####
desc=
--------------------------------------------------------
token=####3001####
english=####Family Photo Album####
foreign=####家族のアルバム####
desc=
--------------------------------------------------------
token=####3002####
english=####The ^^^^sims_family_name^^^^ Photo Album####
foreign=####^^^^sims_family_name^^^^家族のアルバム####
desc=as in "The Smith Family Photo Album"
--------------------------------------------------------
token=####3003####
english=####Photo Album####
foreign=####アルバム####
desc=
--------------------------------------------------------
token=####3004####
english=####Page ^^^^sims_scrapbook_currpagenum^^^^ of ^^^^sims_scrapbook_numpages^^^^####
foreign=####^^^^sims_scrapbook_numpages^^^^のページ^^^^sims_scrapbook_currpagenum^^^^####
desc=as in "page 4 of 10"
--------------------------------------------------------
token=####3005####
english=####of####
foreign=####の####
desc=as in "page 4 of 10"
--------------------------------------------------------
token=####3006####
english=####Next####
foreign=####次####
desc=next page
--------------------------------------------------------
token=####3007####
english=####Back####
foreign=####前####
desc=back to the previous page
--------------------------------------------------------
token=####3008####
english=####Empty photo album.####
foreign=####アルバムを空にする####
desc=
--------------------------------------------------------
token=####4001####
english=####My Neighborhood Web Pages####
foreign=####私の近所のウェブサイト####
desc=
--------------------------------------------------------
token=####4002####
english=####Address Book####
foreign=####アドレス帳####
desc=
--------------------------------------------------------
token=####4003####
english=####Family Name####
foreign=####家族名####
desc=
--------------------------------------------------------
token=####4004####
english=####Address####
foreign=####アドレス####
desc=House address
--------------------------------------------------------
token=####4005####
english=####Unlisted####
foreign=####リストにありません####
desc=An unlisted address in the address book
--------------------------------------------------------
token=####5001####
english=####The ^^^^sims_family_name^^^^ Family####
foreign=####^^^^sims_family_name^^^^家族####
desc=don't translate the part in between the ^^^^ symbols, that will be replaced with the real family name.
--------------------------------------------------------
token=####5002####
english=####Loading...####
foreign=####ロード中…####
desc=waiting for a web page to load
--------------------------------------------------------
token=####5003####
english=####My Family Web Pages####
foreign=####私の家族のウェブページ####
desc=
--------------------------------------------------------
token=####5004####
english=####Page 1 of 1####
foreign=####ページ１/１####
desc=
--------------------------------------------------------
token=####6001####
english=####Number ^^^^sims_neighborhood_familyX_lot^^^^ Sim Lane:####
foreign=####^^^^sims_neighborhood_familyX_lot^^^^ 番シムレーン：####
desc=
--------------------------------------------------------
token=####6002####
english=####Page ' + current + ' of ' + total + '####
foreign=####' + current + 'のページ' + total + '####
desc=as in "page 4 of 10"
--------------------------------------------------------

