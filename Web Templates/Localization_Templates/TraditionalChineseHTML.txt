token=####1####
english=####The ^^^^sims_family_name^^^^ Family Home Page####
foreign=####^^^^sims_family_name^^^^家庭首頁####
desc=for example "The Smith Family Home Page"
--------------------------------------------------------
token=####2####
english=####Family Home Page####
foreign=####家庭首頁####
desc=
--------------------------------------------------------
token=####3####
english=####Address:####
foreign=####地址：####
desc=
--------------------------------------------------------
token=####4####
english=####^^^^sims_house_address^^^^ Sim Lane####
foreign=####模擬巷道^^^^sims_house_address^^^^####
desc=
--------------------------------------------------------
token=####5####
english=####Cash Balance:####
foreign=####收支平衡：####
desc=amount of spending money your family has
--------------------------------------------------------
token=####6####
english=####Days in Existence:####
foreign=####居住天數：####
desc=number of days your family has lived in this house
--------------------------------------------------------
token=####7####
english=####Number of Family Members:####
foreign=####家庭成員數：####
desc=
--------------------------------------------------------
token=####8####
english=####Number of Family Friends:####
foreign=####家庭友人數：####
desc=
--------------------------------------------------------
token=####9####
english=####See the house.####
foreign=####檢視房屋。####
desc=
--------------------------------------------------------
token=####10####
english=####This family is homeless.####
foreign=####此家庭目前沒有房屋。####
desc=
--------------------------------------------------------
token=####11####
english=####See the neighborhood.####
foreign=####檢視社區。####
desc=
--------------------------------------------------------
token=####12####
english=####House####
foreign=####房屋####
desc=
--------------------------------------------------------
token=####1001####
english=####Number ^^^^sims_house_address^^^^ Sim Lane:####
foreign=####模擬巷道^^^^sims_house_address^^^^號：####
desc=
--------------------------------------------------------
token=####1002####
english=####Sim Lane:####
foreign=####模擬巷道：####
desc=
--------------------------------------------------------
token=####1003####
english=####Address:####
foreign=####地址：####
desc=house address
--------------------------------------------------------
token=####1004####
english=####The ^^^^sims_family_name^^^^ Family Home####
foreign=####^^^^sims_family_name^^^^家庭房屋####
desc=As in "The Smith Family Home"
--------------------------------------------------------
token=####1005####
english=####Family Home####
foreign=####家庭房屋####
desc=
--------------------------------------------------------
token=####1006####
english=####Information####
foreign=####資訊####
desc=
--------------------------------------------------------
token=####1007####
english=####Value:####
foreign=####房價：####
desc=cash value of the house
--------------------------------------------------------
token=####1008####
english=####Square Feet:####
foreign=####平方呎：####
desc=floor space of the house measured in US feet
--------------------------------------------------------
token=####1009####
english=####Number of Floors:####
foreign=####樓層數：####
desc=number of levels/stories in the house
--------------------------------------------------------
token=####1010####
english=####Number of Bathrooms:####
foreign=####廁所數：####
desc=
--------------------------------------------------------
token=####1011####
english=####Number of Bedrooms:####
foreign=####臥室數：####
desc=
--------------------------------------------------------
token=####1012####
english=####Statistics####
foreign=####統計資料####
desc=
--------------------------------------------------------
token=####1013####
english=####Size:####
foreign=####大小：####
desc=
--------------------------------------------------------
token=####1014####
english=####Furnishings:####
foreign=####傢俱：####
desc=
--------------------------------------------------------
token=####1015####
english=####Yard:####
foreign=####碼：####
desc=
--------------------------------------------------------
token=####1016####
english=####Upkeep:####
foreign=####維修費：####
desc=
--------------------------------------------------------
token=####1017####
english=####Layout:####
foreign=####佈局：####
desc=
--------------------------------------------------------
token=####1018####
english=####Exterior####
foreign=####外觀####
desc=
--------------------------------------------------------
token=####1019####
english=####First Floor Interior####
foreign=####一樓內觀####
desc=
--------------------------------------------------------
token=####1020####
english=####Second Floor Interior####
foreign=####二樓內觀####
desc=
--------------------------------------------------------
token=####2001####
english=####Bio####
foreign=####傳記####
desc=
--------------------------------------------------------
token=####2002####
english=####Biography####
foreign=####傳記####
desc=
--------------------------------------------------------
token=####2003####
english=####Page ^^^^sims_scrapbook_currpagenum^^^^ of ^^^^sims_scrapbook_numpages^^^^####
foreign=####^^^^sims_scrapbook_numpages^^^^之^^^^sims_scrapbook_currpagenum^^^^頁####
desc=shows which page your on out of the total pages in the address book
--------------------------------------------------------
token=####2004####
english=####Family####
foreign=####家庭####
desc=
--------------------------------------------------------
token=####2005####
english=####Family Member####
foreign=####家庭成員####
desc=
--------------------------------------------------------
token=####2006####
english=####The ^^^^sims_family_name^^^^ Family Home####
foreign=####^^^^sims_family_name^^^^家庭房屋####
desc=
--------------------------------------------------------
token=####2007####
english=####Family:####
foreign=####家庭：####
desc=
--------------------------------------------------------
token=####2008####
english=####Gender:####
foreign=####性別：####
desc=
--------------------------------------------------------
token=####2009####
english=####Kid/Adult:####
foreign=####小孩/成人：####
desc=is this person a kid or an adult
--------------------------------------------------------
token=####2010####
english=####Astrological Sign:####
foreign=####星座：####
desc=english zodiac sign like Leo, Libra, Aquarious, etc
--------------------------------------------------------
token=####2011####
english=####Career Path:####
foreign=####職業別：####
desc=type of career like medical, or military
--------------------------------------------------------
token=####2012####
english=####Current Job:####
foreign=####目前工作：####
desc=
--------------------------------------------------------
token=####2013####
english=####Job Performance:####
foreign=####工作表現：####
desc=
--------------------------------------------------------
token=####2014####
english=####Current Salary:####
foreign=####目前薪資：####
desc=
--------------------------------------------------------
token=####2015####
english=####Neat:####
foreign=####整潔：####
desc=how neat is this person
--------------------------------------------------------
token=####2016####
english=####Outgoing:####
foreign=####傑出：####
desc=how outgoing is this person
--------------------------------------------------------
token=####2017####
english=####Active:####
foreign=####積極：####
desc=how active is this person
--------------------------------------------------------
token=####2018####
english=####Playful:####
foreign=####玩心重：####
desc=how playful is this person
--------------------------------------------------------
token=####2019####
english=####Nice:####
foreign=####友好：####
desc=how nice is this person
--------------------------------------------------------
token=####2020####
english=####Cooking:####
foreign=####廚藝：####
desc=how good at cooking is this person
--------------------------------------------------------
token=####2021####
english=####Mechanical:####
foreign=####技工能力：####
desc=how good at repairing machines is this person
--------------------------------------------------------
token=####2022####
english=####Charisma:####
foreign=####魅力：####
desc=how charismatic is this person
--------------------------------------------------------
token=####2023####
english=####Body:####
foreign=####體能：####
desc=how physically fit is this person
--------------------------------------------------------
token=####2024####
english=####Logic:####
foreign=####邏輯思考力：####
desc=how logical is this person
--------------------------------------------------------
token=####2025####
english=####Creativity:####
foreign=####創造力：####
desc=how creative is this person
--------------------------------------------------------
token=####2026####
english=####Personality####
foreign=####個性####
desc=
--------------------------------------------------------
token=####2027####
english=####Skills####
foreign=####技能####
desc=
--------------------------------------------------------
token=####3001####
english=####Family Photo Album####
foreign=####家庭相簿####
desc=
--------------------------------------------------------
token=####3002####
english=####The ^^^^sims_family_name^^^^ Photo Album####
foreign=####^^^^sims_family_name^^^^相簿####
desc=as in "The Smith Family Photo Album"
--------------------------------------------------------
token=####3003####
english=####Photo Album####
foreign=####相簿####
desc=
--------------------------------------------------------
token=####3004####
english=####Page ^^^^sims_scrapbook_currpagenum^^^^ of ^^^^sims_scrapbook_numpages^^^^####
foreign=####^^^^sims_scrapbook_numpages^^^^之^^^^sims_scrapbook_currpagenum^^^^頁####
desc=as in "page 4 of 10"
--------------------------------------------------------
token=####3005####
english=####of####
foreign=####之####
desc=as in "page 4 of 10"
--------------------------------------------------------
token=####3006####
english=####Next####
foreign=####下一步####
desc=next page
--------------------------------------------------------
token=####3007####
english=####Back####
foreign=####上一步####
desc=back to the previous page
--------------------------------------------------------
token=####3008####
english=####Empty photo album.####
foreign=####空相簿。####
desc=
--------------------------------------------------------
token=####4001####
english=####My Neighborhood Web Pages####
foreign=####我鄰近地區網頁####
desc=
--------------------------------------------------------
token=####4002####
english=####Address Book####
foreign=####通訊錄####
desc=
--------------------------------------------------------
token=####4003####
english=####Family Name####
foreign=####姓####
desc=
--------------------------------------------------------
token=####4004####
english=####Address####
foreign=####地址####
desc=House address
--------------------------------------------------------
token=####4005####
english=####Unlisted####
foreign=####未列出####
desc=An unlisted address in the address book
--------------------------------------------------------
token=####5001####
english=####The ^^^^sims_family_name^^^^ Family####
foreign=#### ^^^^sims_family_name^^^^ 家族####
desc=don't translate the part in between the ^^^^ symbols, that will be replaced with the real family name.
--------------------------------------------------------
token=####5002####
english=####Loading...####
foreign=####載入中…####
desc=waiting for a web page to load
--------------------------------------------------------
token=####5003####
english=####My Family Web Pages####
foreign=####我的家族網頁####
desc=
--------------------------------------------------------
token=####5004####
english=####Page 1 of 1####
foreign=####第一頁####
desc=
--------------------------------------------------------
token=####6001####
english=####Number ^^^^sims_neighborhood_familyX_lot^^^^ Sim Lane:####
foreign=####^^^^sims_neighborhood_familyX_lot^^^^模擬巷道數：####
desc=
--------------------------------------------------------
token=####6002####
english=####Page ' + current + ' of ' + total + '####
foreign=####' + current + '之' + total + '頁####
desc=as in "page 4 of 10"
--------------------------------------------------------

