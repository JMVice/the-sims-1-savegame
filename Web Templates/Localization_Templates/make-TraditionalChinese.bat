REM Builds full templates from the set of template data found on source safe...

ECHO Building TRADITIONAL CHINESE templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\traditional chinese\0_Sim_Lane"
mkdir "..\traditional chinese\1_Sim_Lane"
mkdir "..\traditional chinese\2_Sim_Lane"
mkdir "..\traditional chinese\3_Sim_Lane"
mkdir "..\traditional chinese\4_Sim_Lane"
mkdir "..\traditional chinese\5_Sim_Lane"
mkdir "..\traditional chinese\6_Sim_Lane"
mkdir "..\traditional chinese\7_Sim_Lane"
mkdir "..\traditional chinese\8_Sim_Lane"
mkdir "..\traditional chinese\9_Sim_Lane"
mkdir "..\traditional chinese\10_Sim_Lane"

attrib "..\traditional chinese\0_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\1_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\2_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\3_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\4_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\5_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\6_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\7_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\8_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\9_Sim_Lane\*.*"  -r
attrib "..\traditional chinese\10_Sim_Lane\*.*" -r
attrib "..\traditional chinese\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\0_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\1_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\2_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\3_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\4_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\5_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\6_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\7_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\8_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\9_Sim_Lane"
copy "..\traditional chinese\LotTemplates\*.*"   "..\traditional chinese\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\traditional chinese\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\traditional chinese\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\traditional chinese\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\traditional chinese\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\traditional chinese\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\traditional chinese\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\traditional chinese\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\traditional chinese\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\traditional chinese\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\traditional chinese\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\traditional chinese\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\traditional chinese\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\traditional chinese\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\traditional chinese\NeighborhoodGFX"
