token=####1####
english=####The ^^^^sims_family_name^^^^ Family Home Page####
foreign=####���ྨ�ͧ��ͺ���� ^^^^sims_family_name^^^^####
desc=for example "The Smith Family Home Page"
--------------------------------------------------------
token=####2####
english=####Family Home Page####
foreign=####���ྨ�ͧ��ͺ����####
desc=
--------------------------------------------------------
token=####3####
english=####Address:####
foreign=####�������:####
desc=
--------------------------------------------------------
token=####4####
english=####^^^^sims_house_address^^^^ Sim Lane####
foreign=####^^^^ sims_house_address ^^^^ ����Ź####
desc=
--------------------------------------------------------
token=####5####
english=####Cash Balance:####
foreign=####�Թ��������:####
desc=amount of spending money your family has
--------------------------------------------------------
token=####6####
english=####Days in Existence:####
foreign=####�ӹǹ�ѹ��������:####
desc=number of days your family has lived in this house
--------------------------------------------------------
token=####7####
english=####Number of Family Members:####
foreign=####�ӹǹ��Ҫԡ㹤�ͺ����:####
desc=
--------------------------------------------------------
token=####8####
english=####Number of Family Friends:####
foreign=####�ӹǹ���͹�ͧ��ͺ����:####
desc=
--------------------------------------------------------
token=####9####
english=####See the house.####
foreign=####����ҹ####
desc=
--------------------------------------------------------
token=####10####
english=####This family is homeless.####
foreign=####��ͺ���ǹ�����������####
desc=
--------------------------------------------------------
token=####11####
english=####See the neighborhood.####
foreign=####�����͹��ҹ####
desc=
--------------------------------------------------------
token=####12####
english=####House####
foreign=####��ҹ####
desc=
--------------------------------------------------------
token=####1001####
english=####Number ^^^^sims_house_address^^^^ Sim Lane:####
foreign=####Number ^^^^sims_house_address^^^^ ����Ź:####
desc=
--------------------------------------------------------
token=####1002####
english=####Sim Lane:####
foreign=####����Ź:####
desc=
--------------------------------------------------------
token=####1003####
english=####Address:####
foreign=####�������:####
desc=house address
--------------------------------------------------------
token=####1004####
english=####The ^^^^sims_family_name^^^^ Family Home####
foreign=####��ҹ�ͧ��ͺ���� ^^^^sims_family_name^^^^ ####
desc=As in "The Smith Family Home"
--------------------------------------------------------
token=####1005####
english=####Family Home####
foreign=####��ҹ�ͧ��ͺ����####
desc=
--------------------------------------------------------
token=####1006####
english=####Information####
foreign=####������####
desc=
--------------------------------------------------------
token=####1007####
english=####Value:####
foreign=####��Ť��:####
desc=cash value of the house
--------------------------------------------------------
token=####1008####
english=####Square Feet:####
foreign=####���ҧ�ص:####
desc=floor space of the house measured in US feet
--------------------------------------------------------
token=####1009####
english=####Number of Floors:####
foreign=####�ӹǹ���:####
desc=number of levels/stories in the house
--------------------------------------------------------
token=####1010####
english=####Number of Bathrooms:####
foreign=####�ӹǹ��ͧ���:####
desc=
--------------------------------------------------------
token=####1011####
english=####Number of Bedrooms:####
foreign=####�ӹǹ��ͧ�͹:####
desc=
--------------------------------------------------------
token=####1012####
english=####Statistics####
foreign=####ʶԵ�####
desc=
--------------------------------------------------------
token=####1013####
english=####Size:####
foreign=####��Ҵ:####
desc=
--------------------------------------------------------
token=####1014####
english=####Furnishings:####
foreign=####���������:####
desc=
--------------------------------------------------------
token=####1015####
english=####Yard:####
foreign=####ʹ��:####
desc=
--------------------------------------------------------
token=####1016####
english=####Upkeep:####
foreign=####��Һ��ا�ѡ��:####
desc=
--------------------------------------------------------
token=####1017####
english=####Layout:####
foreign=####�ٻẺ:####
desc=
--------------------------------------------------------
token=####1018####
english=####Exterior####
foreign=####��¹͡####
desc=
--------------------------------------------------------
token=####1019####
english=####First Floor Interior####
foreign=####���㹪��˹��####
desc=
--------------------------------------------------------
token=####1020####
english=####Second Floor Interior####
foreign=####���㹪���ͧ####
desc=
--------------------------------------------------------
token=####2001####
english=####Bio####
foreign=####��ǻ���ѵ�####
desc=short for biography
--------------------------------------------------------
token=####2002####
english=####Biography####
foreign=####��ǻ���ѵ�####
desc=
--------------------------------------------------------
token=####2003####
english=####Page ^^^^sims_scrapbook_currpagenum^^^^ of ^^^^sims_scrapbook_numpages^^^^####
foreign=####˹�� ^^^^sims_scrapbook_currpagenum^^^^ �ҡ ^^^^sims_scrapbook_numpages^^^^####
desc=shows which page your on out of the total pages in the address book
--------------------------------------------------------
token=####2004####
english=####Family####
foreign=####��ͺ����####
desc=
--------------------------------------------------------
token=####2005####
english=####Family Member####
foreign=####��Ҫԡ㹤�ͺ����####
desc=
--------------------------------------------------------
token=####2006####
english=####The ^^^^sims_family_name^^^^ Family Home####
foreign=####��ҹ�ͧ��ͺ���� ^^^^sims_family_name^^^^####
desc=
--------------------------------------------------------
token=####2007####
english=####Family:####
foreign=####��ͺ����:####
desc=
--------------------------------------------------------
token=####2008####
english=####Gender:####
foreign=####��:####
desc=
--------------------------------------------------------
token=####2009####
english=####Kid/Adult:####
foreign=####��/����˭�:####
desc=is this person a kid or an adult
--------------------------------------------------------
token=####2010####
english=####Astrological Sign:####
foreign=####����:####
desc=english zodiac sign like Leo, Libra, Aquarious, etc
--------------------------------------------------------
token=####2011####
english=####Career Path:####
foreign=####��鹷ҧ�Ҫվ:####
desc=type of career like medical, or military
--------------------------------------------------------
token=####2012####
english=####Current Job:####
foreign=####�ҹ�Ѩ�غѹ:####
desc=
--------------------------------------------------------
token=####2013####
english=####Job Performance:####
foreign=####�ŧҹ:####
desc=
--------------------------------------------------------
token=####2014####
english=####Current Salary:####
foreign=####�Թ��͹�Ѩ�غѹ:####
desc=
--------------------------------------------------------
token=####2015####
english=####Neat:####
foreign=####��гյ:####
desc=how neat is this person
--------------------------------------------------------
token=####2016####
english=####Outgoing:####
foreign=####�ͺ�ʴ��͡:####
desc=how outgoing is this person
--------------------------------------------------------
token=####2017####
english=####Active:####
foreign=####��е�������:####
desc=how active is this person
--------------------------------------------------------
token=####2018####
english=####Playful:####
foreign=####������:####
desc=how playful is this person
--------------------------------------------------------
token=####2019####
english=####Nice:####
foreign=####㨴�:####
desc=how nice is this person
--------------------------------------------------------
token=####2020####
english=####Cooking:####
foreign=####��ا�����:####
desc=how good at cooking is this person
--------------------------------------------------------
token=####2021####
english=####Mechanical:####
foreign=####��ê�ҧ:####
desc=how good at repairing machines is this person
--------------------------------------------------------
token=####2022####
english=####Charisma:####
foreign=####�ʹ���:####
desc=how charismatic is this person
--------------------------------------------------------
token=####2023####
english=####Body:####
foreign=####��ҧ���:####
desc=how physically fit is this person
--------------------------------------------------------
token=####2024####
english=####Logic:####
foreign=####��á�:####
desc=how logical is this person
--------------------------------------------------------
token=####2025####
english=####Creativity:####
foreign=####�������ҧ��ä�:####
desc=how creative is this person
--------------------------------------------------------
token=####2026####
english=####Personality####
foreign=####�ؤ�ԡ####
desc=
--------------------------------------------------------
token=####2027####
english=####Skills####
foreign=####�ѡ��####
desc=
--------------------------------------------------------
token=####3001####
english=####Family Photo Album####
foreign=####��ź����ٻ���¢ͧ��ͺ����####
desc=
--------------------------------------------------------
token=####3002####
english=####The ^^^^sims_family_name^^^^ Photo Album####
foreign=####��ź����ٻ���¢ͧ��ͺ���� ^^^^sims_family_name^^^^####
desc=as in "The Smith Family Photo Album"
--------------------------------------------------------
token=####3003####
english=####Photo Album####
foreign=####��ź����ٻ����####
desc=
--------------------------------------------------------
token=####3004####
english=####Page ^^^^sims_scrapbook_currpagenum^^^^ of ^^^^sims_scrapbook_numpages^^^^####
foreign=####˹�� ^^^^sims_scrapbook_currpagenum^^^^ �ҡ^^^^sims_scrapbook_numpages^^^^####
desc=as in "page 4 of 10"
--------------------------------------------------------
token=####3005####
english=####of####
foreign=####�ҡ####
desc=as in "page 4 of 10"
--------------------------------------------------------
token=####3006####
english=####Next####
foreign=####�Ѵ�####
desc=next page
--------------------------------------------------------
token=####3007####
english=####Back####
foreign=####��͹��Ѻ####
desc=back to the previous page
--------------------------------------------------------
token=####3008####
english=####Empty photo album.####
foreign=####��ź����ٻ������ҧ####
desc=
--------------------------------------------------------
token=####4001####
english=####My Neighborhood Web Pages####
foreign=####���ྨ�ͧᶺ������������####
desc=
--------------------------------------------------------
token=####4002####
english=####Address Book####
foreign=####��ش�������####
desc=
--------------------------------------------------------
token=####4003####
english=####Family Name####
foreign=####���ͤ�ͺ����####
desc=
--------------------------------------------------------
token=####4004####
english=####Address####
foreign=####�������####
desc=House address
--------------------------------------------------------
token=####4005####
english=####Unlisted####
foreign=####�����ѹ�֡���####
desc=An unlisted address in the address book
--------------------------------------------------------
token=####5001####
english=####The ^^^^sims_family_name^^^^ Family####
foreign=####The ^^^^sims_family_name^^^^ Family####
desc=don't translate the part in between the ^^^^ symbols, that will be replaced with the real family name.
--------------------------------------------------------
token=####5002####
english=####Loading...####
foreign=####���ѧ��Ŵ...####
desc=waiting for a web page to load
--------------------------------------------------------
token=####5003####
english=####My Family Web Pages####
foreign=####���ྨ�ͧ��ͺ����####
desc=
--------------------------------------------------------
token=####5004####
english=####Page 1 of 1####
foreign=####˹�� 1 � 1####
desc=
--------------------------------------------------------
token=####6001####
english=####Number ^^^^sims_neighborhood_familyX_lot^^^^ Sim Lane:####
foreign=####Number ^^^^sims_neighborhood_familyX_lot^^^^ ����Ź:####
desc=
--------------------------------------------------------
token=####6002####
english=####Page ' + current + ' of ' + total + '####
foreign=####˹�� ' + current + ' �ҡ' + total + '####
desc=as in "page 4 of 10"
--------------------------------------------------------

