ECHO Makes all language templates from the SS data

make-dutch.bat
make-english.bat
make-englishUK.bat
make-french.bat
make-german.bat
make-italian.bat
make-japanese.bat
make-korean.bat
make-polish.bat
make-portuguese.bat
make-SimplifiedChinese.bat
make-spanish.bat
make-swedish.bat
make-thai.bat
make-TraditionalChinese.bat
