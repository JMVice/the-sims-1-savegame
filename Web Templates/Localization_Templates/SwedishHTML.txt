token=####1####
english=####The ^^^^sims_family_name^^^^ Family Home Page####
foreign=####Familjen ^^^^sims_family_name^^^^s hemsida####
desc=for example "The Smith Family Home Page"
--------------------------------------------------------
token=####2####
english=####Family Home Page####
foreign=####Familjens hemsida####
desc=
--------------------------------------------------------
token=####3####
english=####Address:####
foreign=####Adress:####
desc=
--------------------------------------------------------
token=####4####
english=####^^^^sims_house_address^^^^ Sim Lane####
foreign=####Simgatan ^^^^sims_house_address^^^^####
desc=
--------------------------------------------------------
token=####5####
english=####Cash Balance:####
foreign=####Kontobalans:####
desc=amount of spending money your family has
--------------------------------------------------------
token=####6####
english=####Days in Existence:####
foreign=####Dagar sedan skapandet:####
desc=number of days your family has lived in this house
--------------------------------------------------------
token=####7####
english=####Number of Family Members:####
foreign=####Antal familjemedlemmar:####
desc=
--------------------------------------------------------
token=####8####
english=####Number of Family Friends:####
foreign=####Antal v�nner till familjen:####
desc=
--------------------------------------------------------
token=####9####
english=####See the house.####
foreign=####Titta p� huset.####
desc=
--------------------------------------------------------
token=####10####
english=####This family is homeless.####
foreign=####Den h�r familjen �r bostadsl�s.####
desc=
--------------------------------------------------------
token=####11####
english=####See the neighborhood.####
foreign=####Titta p� grannskapet.####
desc=
--------------------------------------------------------
token=####12####
english=####House####
foreign=####Hus####
desc=
--------------------------------------------------------
token=####1001####
english=####Number ^^^^sims_house_address^^^^ Sim Lane:####
foreign=####Simgatan ^^^^sims_house_address^^^^:####
desc=
--------------------------------------------------------
token=####1002####
english=####Sim Lane:####
foreign=####Simgatan:####
desc=
--------------------------------------------------------
token=####1003####
english=####Address:####
foreign=####Adress:####
desc=house address
--------------------------------------------------------
token=####1004####
english=####The ^^^^sims_family_name^^^^ Family Home####
foreign=####Familjen ^^^^sims_family_name^^^^s hem####
desc=As in "The Smith Family Home"
--------------------------------------------------------
token=####1005####
english=####Family Home####
foreign=####Familjens hem####
desc=
--------------------------------------------------------
token=####1006####
english=####Information####
foreign=####Information####
desc=
--------------------------------------------------------
token=####1007####
english=####Value:####
foreign=####V�rde:####
desc=cash value of the house
--------------------------------------------------------
token=####1008####
english=####Square Feet:####
foreign=####Kvadratmeter:####
desc=floor space of the house measured in US feet
--------------------------------------------------------
token=####1009####
english=####Number of Floors:####
foreign=####Antal v�ningar:####
desc=number of levels/stories in the house
--------------------------------------------------------
token=####1010####
english=####Number of Bathrooms:####
foreign=####Antal badrum:####
desc=
--------------------------------------------------------
token=####1011####
english=####Number of Bedrooms:####
foreign=####Antal sovrum:####
desc=
--------------------------------------------------------
token=####1012####
english=####Statistics####
foreign=####Statistik####
desc=
--------------------------------------------------------
token=####1013####
english=####Size:####
foreign=####Storlek:####
desc=
--------------------------------------------------------
token=####1014####
english=####Furnishings:####
foreign=####M�bler:####
desc=
--------------------------------------------------------
token=####1015####
english=####Yard:####
foreign=####Tr�dg�rd:####
desc=
--------------------------------------------------------
token=####1016####
english=####Upkeep:####
foreign=####Underh�ll:####
desc=
--------------------------------------------------------
token=####1017####
english=####Layout:####
foreign=####Layout:####
desc=
--------------------------------------------------------
token=####1018####
english=####Exterior####
foreign=####Exteri�r####
desc=
--------------------------------------------------------
token=####1019####
english=####First Floor Interior####
foreign=####Interi�r, f�rsta v�ningen####
desc=
--------------------------------------------------------
token=####1020####
english=####Second Floor Interior####
foreign=####Interi�r, andra v�ningen####
desc=
--------------------------------------------------------
token=####2001####
english=####Bio####
foreign=####Bio####
desc=short for biography
--------------------------------------------------------
token=####2002####
english=####Biography####
foreign=####Biografi####
desc=
--------------------------------------------------------
token=####2003####
english=####Page ^^^^sims_scrapbook_currpagenum^^^^ of ^^^^sims_scrapbook_numpages^^^^####
foreign=####Sida ^^^^sims_scrapbook_currpagenum^^^^ av ^^^^sims_scrapbook_numpages^^^^####
desc=shows which page your on out of the total pages in the address book
--------------------------------------------------------
token=####2004####
english=####Family####
foreign=####Familj####
desc=
--------------------------------------------------------
token=####2005####
english=####Family Member####
foreign=####Familjemedlem####
desc=
--------------------------------------------------------
token=####2006####
english=####The ^^^^sims_family_name^^^^ Family Home####
foreign=####Familjen ^^^^sims_family_name^^^^s hem####
desc=
--------------------------------------------------------
token=####2007####
english=####Family:####
foreign=####Familj:####
desc=
--------------------------------------------------------
token=####2008####
english=####Gender:####
foreign=####K�n:####
desc=
--------------------------------------------------------
token=####2009####
english=####Kid/Adult:####
foreign=####Barn/Vuxen:####
desc=is this person a kid or an adult
--------------------------------------------------------
token=####2010####
english=####Astrological Sign:####
foreign=####Stj�rntecken:####
desc=english zodiac sign like Leo, Libra, Aquarious, etc
--------------------------------------------------------
token=####2011####
english=####Career Path:####
foreign=####Karri�r:####
desc=type of career like medical, or military
--------------------------------------------------------
token=####2012####
english=####Current Job:####
foreign=####Nuvarande jobb:####
desc=
--------------------------------------------------------
token=####2013####
english=####Job Performance:####
foreign=####Arbetsprestation:####
desc=
--------------------------------------------------------
token=####2014####
english=####Current Salary:####
foreign=####Nuvarande l�n:####
desc=
--------------------------------------------------------
token=####2015####
english=####Neat:####
foreign=####Ordning:####
desc=how neat is this person
--------------------------------------------------------
token=####2016####
english=####Outgoing:####
foreign=####Ut�triktad:####
desc=how outgoing is this person
--------------------------------------------------------
token=####2017####
english=####Active:####
foreign=####Aktiv:####
desc=how active is this person
--------------------------------------------------------
token=####2018####
english=####Playful:####
foreign=####Lekfull:####
desc=how playful is this person
--------------------------------------------------------
token=####2019####
english=####Nice:####
foreign=####Trevlig:####
desc=how nice is this person
--------------------------------------------------------
token=####2020####
english=####Cooking:####
foreign=####Matlagning:####
desc=how good at cooking is this person
--------------------------------------------------------
token=####2021####
english=####Mechanical:####
foreign=####Teknik:####
desc=how good at repairing machines is this person
--------------------------------------------------------
token=####2022####
english=####Charisma:####
foreign=####Karisma:####
desc=how charismatic is this person
--------------------------------------------------------
token=####2023####
english=####Body:####
foreign=####Fysik:####
desc=how physically fit is this person
--------------------------------------------------------
token=####2024####
english=####Logic:####
foreign=####Logik:####
desc=how logical is this person
--------------------------------------------------------
token=####2025####
english=####Creativity:####
foreign=####Kreativitet:####
desc=how creative is this person
--------------------------------------------------------
token=####2026####
english=####Personality####
foreign=####Personlighet####
desc=
--------------------------------------------------------
token=####2027####
english=####Skills####
foreign=####F�rdigheter####
desc=
--------------------------------------------------------
token=####3001####
english=####Family Photo Album####
foreign=####Familjens fotoalbum####
desc=
--------------------------------------------------------
token=####3002####
english=####The ^^^^sims_family_name^^^^ Photo Album####
foreign=####Familjen ^^^^sims_family_name^^^^s fotoalbum####
desc=as in "The Smith Family Photo Album"
--------------------------------------------------------
token=####3003####
english=####Photo Album####
foreign=####Fotoalbum####
desc=
--------------------------------------------------------
token=####3004####
english=####Page ^^^^sims_scrapbook_currpagenum^^^^ of ^^^^sims_scrapbook_numpages^^^^####
foreign=####Sida ^^^^sims_scrapbook_currpagenum^^^^ av ^^^^sims_scrapbook_numpages^^^^####
desc=as in "page 4 of 10"
--------------------------------------------------------
token=####3005####
english=####of####
foreign=####av####
desc=as in "page 4 of 10"
--------------------------------------------------------
token=####3006####
english=####Next####
foreign=####N�sta####
desc=next page
--------------------------------------------------------
token=####3007####
english=####Back####
foreign=####Tillbaka####
desc=back to the previous page
--------------------------------------------------------
token=####3008####
english=####Empty photo album.####
foreign=####Tomt fotoalbum.####
desc=
--------------------------------------------------------
token=####4001####
english=####My Neighborhood Web Pages####
foreign=####Mitt grannskaps webbsidor####
desc=
--------------------------------------------------------
token=####4002####
english=####Address Book####
foreign=####Adressbok####
desc=
--------------------------------------------------------
token=####4003####
english=####Family Name####
foreign=####Familjens namn####
desc=
--------------------------------------------------------
token=####4004####
english=####Address####
foreign=####Adress####
desc=House address
--------------------------------------------------------
token=####4005####
english=####Unlisted####
foreign=####Ej listad####
desc=An unlisted address in the address book
--------------------------------------------------------
token=####5001####
english=####The ^^^^sims_family_name^^^^ Family####
foreign=####Familjen ^^^^sims_family_name^^^^ ####
desc=don't translate the part in between the ^^^^ symbols, that will be replaced with the real family name.
--------------------------------------------------------
token=####5002####
english=####Loading...####
foreign=####Laddar...####
desc=waiting for a web page to load
--------------------------------------------------------
token=####5003####
english=####My Family Web Pages####
foreign=####Min familjs hemsida####
desc=
--------------------------------------------------------
token=####5004####
english=####Page 1 of 1####
foreign=####Sida 1 av 1####
desc=
--------------------------------------------------------
token=####6001####
english=####Number ^^^^sims_neighborhood_familyX_lot^^^^ Sim Lane:####
foreign=####Simgatan ^^^^sims_neighborhood_familyX_lot^^^^:####
desc=
--------------------------------------------------------
token=####6002####
english=####Page ' + current + ' of ' + total + '####
foreign=####Sida ' + current + ' av ' + total + '####
desc=as in "page 4 of 10"
--------------------------------------------------------
