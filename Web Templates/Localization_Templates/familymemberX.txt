token=####2001####
english=####Bio####
foreign=####Bio####
desc=short for biography
--------------------------------------------------------
token=####2002####
english=####Biography####
foreign=####Biography####
desc=
--------------------------------------------------------
token=####2003####
english=####Page####
foreign=####Page####
desc=web page
--------------------------------------------------------
token=####2004####
english=####Family####
foreign=####Family####
desc=
--------------------------------------------------------
token=####2005####
english=####Family Member####
foreign=####Family Member####
desc=
--------------------------------------------------------
token=####2006####
english=####The####
foreign=####The####
desc=
--------------------------------------------------------
token=####2007####
english=####Family:####
foreign=####Family:####
desc=
--------------------------------------------------------
token=####2008####
english=####Gender:####
foreign=####Gender:####
desc=
--------------------------------------------------------
token=####2009####
english=####Kid/Adult:####
foreign=####Kid/Adult:####
desc=is this person a kid or an adult
--------------------------------------------------------
token=####2010####
english=####Astrological Sign:####
foreign=####Astrological Sign:####
desc=english zodiac sign like Leo, Libra, Aquarious, etc
--------------------------------------------------------
token=####2011####
english=####Career Path:####
foreign=####Career Path:####
desc=type of career like medical, or military
--------------------------------------------------------
token=####2012####
english=####Current Job:####
english=####Current Job:####
desc=
--------------------------------------------------------
token=####2013####
english=####Job Performance:####
foreign=####Job Performance:####
desc=
--------------------------------------------------------
token=####2014####
english=####Current Salary:####
foreign=####Current Salary:####
desc=
--------------------------------------------------------
token=####2015####
english=####Neat:####
foreign=####Neat:####
desc=how neat is this person
--------------------------------------------------------
token=####2016####
english=####Outgoing:####
foreign=####Outgoing:####
desc=how outgoing is this person
--------------------------------------------------------
token=####2017####
english=####Active:####
foreign=####Active:####
desc=how active is this person
--------------------------------------------------------
token=####2018####
english=####Playful:####
foreign=####Playful:####
desc=how playful is this person
--------------------------------------------------------
token=####2019####
english=####Nice:####
foreign=####Nice:####
desc=how nice is this person
--------------------------------------------------------
token=####2020####
english=####Cooking:####
foreign=####Cooking:####
desc=how good at cooking is this person
--------------------------------------------------------
token=####2021####
english=####Mechanical:####
foreign=####Mechanical:####
desc=how good at repairing machines is this person
--------------------------------------------------------
token=####2022####
english=####Charisma:####
foreign=####Charisma:####
desc=how charismatic is this person
--------------------------------------------------------
token=####2023####
english=####Body:####
foreign=####Body:####
desc=how physically fit is this person
--------------------------------------------------------
token=####2024####
english=####Logic:####
foreign=####Logic:####
desc=how logical is this person
--------------------------------------------------------
token=####2025####
english=####Creativity:####
foreign=####Creativity:####
desc=how creative is this person
--------------------------------------------------------
token=####2026####
english=####Personality####
foreign=####Personality####
desc=
--------------------------------------------------------
token=####2027####
english=####Skills####
foreign=####Skills####
desc=
--------------------------------------------------------
