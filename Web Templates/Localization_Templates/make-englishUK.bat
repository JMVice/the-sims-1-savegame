REM Builds full templates from the set of template data found on source safe...

ECHO Building ENGLISH UK templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\english uk\0_Sim_Lane"
mkdir "..\english uk\1_Sim_Lane"
mkdir "..\english uk\2_Sim_Lane"
mkdir "..\english uk\3_Sim_Lane"
mkdir "..\english uk\4_Sim_Lane"
mkdir "..\english uk\5_Sim_Lane"
mkdir "..\english uk\6_Sim_Lane"
mkdir "..\english uk\7_Sim_Lane"
mkdir "..\english uk\8_Sim_Lane"
mkdir "..\english uk\9_Sim_Lane"
mkdir "..\english uk\10_Sim_Lane"

attrib "..\english uk\0_Sim_Lane\*.*"  -r
attrib "..\english uk\1_Sim_Lane\*.*"  -r
attrib "..\english uk\2_Sim_Lane\*.*"  -r
attrib "..\english uk\3_Sim_Lane\*.*"  -r
attrib "..\english uk\4_Sim_Lane\*.*"  -r
attrib "..\english uk\5_Sim_Lane\*.*"  -r
attrib "..\english uk\6_Sim_Lane\*.*"  -r
attrib "..\english uk\7_Sim_Lane\*.*"  -r
attrib "..\english uk\8_Sim_Lane\*.*"  -r
attrib "..\english uk\9_Sim_Lane\*.*"  -r
attrib "..\english uk\10_Sim_Lane\*.*" -r
attrib "..\english uk\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\english uk\LotTemplates\*.*"   "..\english uk\0_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\1_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\2_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\3_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\4_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\5_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\6_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\7_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\8_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\9_Sim_Lane"
copy "..\english uk\LotTemplates\*.*"   "..\english uk\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\english uk\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english uk\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\english uk\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\english uk\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\english uk\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\english uk\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\english uk\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\english uk\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\english uk\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\english uk\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\english uk\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\english uk\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\english uk\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\english uk\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\english uk\NeighborhoodGFX"
