REM Builds full templates from the set of template data found on source safe...

ECHO Building JAPANESE templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\japanese\0_Sim_Lane"
mkdir "..\japanese\1_Sim_Lane"
mkdir "..\japanese\2_Sim_Lane"
mkdir "..\japanese\3_Sim_Lane"
mkdir "..\japanese\4_Sim_Lane"
mkdir "..\japanese\5_Sim_Lane"
mkdir "..\japanese\6_Sim_Lane"
mkdir "..\japanese\7_Sim_Lane"
mkdir "..\japanese\8_Sim_Lane"
mkdir "..\japanese\9_Sim_Lane"
mkdir "..\japanese\10_Sim_Lane"

attrib "..\japanese\0_Sim_Lane\*.*"  -r
attrib "..\japanese\1_Sim_Lane\*.*"  -r
attrib "..\japanese\2_Sim_Lane\*.*"  -r
attrib "..\japanese\3_Sim_Lane\*.*"  -r
attrib "..\japanese\4_Sim_Lane\*.*"  -r
attrib "..\japanese\5_Sim_Lane\*.*"  -r
attrib "..\japanese\6_Sim_Lane\*.*"  -r
attrib "..\japanese\7_Sim_Lane\*.*"  -r
attrib "..\japanese\8_Sim_Lane\*.*"  -r
attrib "..\japanese\9_Sim_Lane\*.*"  -r
attrib "..\japanese\10_Sim_Lane\*.*" -r
attrib "..\japanese\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\japanese\LotTemplates\*.*"   "..\japanese\0_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\1_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\2_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\3_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\4_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\5_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\6_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\7_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\8_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\9_Sim_Lane"
copy "..\japanese\LotTemplates\*.*"   "..\japanese\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\japanese\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\japanese\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\japanese\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\japanese\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\japanese\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\japanese\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\japanese\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\japanese\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\japanese\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\japanese\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\japanese\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\japanese\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\japanese\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\japanese\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\japanese\NeighborhoodGFX"
