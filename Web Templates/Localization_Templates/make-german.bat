REM Builds full templates from the set of template data found on source safe...

ECHO Building GERMAN templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\german\0_Sim_Lane"
mkdir "..\german\1_Sim_Lane"
mkdir "..\german\2_Sim_Lane"
mkdir "..\german\3_Sim_Lane"
mkdir "..\german\4_Sim_Lane"
mkdir "..\german\5_Sim_Lane"
mkdir "..\german\6_Sim_Lane"
mkdir "..\german\7_Sim_Lane"
mkdir "..\german\8_Sim_Lane"
mkdir "..\german\9_Sim_Lane"
mkdir "..\german\10_Sim_Lane"

attrib "..\german\0_Sim_Lane\*.*"  -r
attrib "..\german\1_Sim_Lane\*.*"  -r
attrib "..\german\2_Sim_Lane\*.*"  -r
attrib "..\german\3_Sim_Lane\*.*"  -r
attrib "..\german\4_Sim_Lane\*.*"  -r
attrib "..\german\5_Sim_Lane\*.*"  -r
attrib "..\german\6_Sim_Lane\*.*"  -r
attrib "..\german\7_Sim_Lane\*.*"  -r
attrib "..\german\8_Sim_Lane\*.*"  -r
attrib "..\german\9_Sim_Lane\*.*"  -r
attrib "..\german\10_Sim_Lane\*.*" -r
attrib "..\german\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\german\LotTemplates\*.*"   "..\german\0_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\1_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\2_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\3_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\4_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\5_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\6_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\7_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\8_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\9_Sim_Lane"
copy "..\german\LotTemplates\*.*"   "..\german\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\german\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\german\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\german\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\german\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\german\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\german\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\german\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\german\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\german\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\german\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\german\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\german\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\german\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\german\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\german\NeighborhoodGFX"
