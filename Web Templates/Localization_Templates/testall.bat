REM -=- Test templates for errors -=-

ECHO -========- generating DUTCH web templates -========-
tokin familyhome.html     dutchHTML.txt               "tmp/familyhome.html" >> testall.log


ECHO -=======- generating ENGLISH web templates -=======-
tokin familyhome.html     englishHTML.txt             "tmp/familyhome.html" >> testall.log


ECHO -=====- generating ENGLISH UK web templates  -=====-
tokin familyhome.html     englishukHTML.txt           "tmp/familyhome.html" >> testall.log

ECHO -=======- generating FRENCH web templates  -=======-
tokin familyhome.html     frenchHTML.txt              "tmp/familyhome.html" >> testall.log


ECHO -=======- generating GERMAN web templates  -=======-
tokin familyhome.html     germanHTML.txt              "tmp/familyhome.html" >> testall.log


ECHO -=======- generating ITALIAN web templates -=======-
tokin familyhome.html     italianHTML.txt             "tmp/familyhome.html" >> testall.log


ECHO -======- generating JAPANESE web templates  -======-
tokin familyhome.html     japaneseHTML.txt            "tmp/familyhome.html" >> testall.log


ECHO -=======- generating KOREAN web templates  -=======-
tokin familyhome.html     koreanHTML.txt              "tmp/familyhome.html" >> testall.log


ECHO -=======- generating POLISH web templates  -=======-
tokin familyhome.html     polishHTML.txt              "tmp/familyhome.html" >> testall.log


ECHO -=====- generating PORTUGUESE web templates  -=====-
tokin familyhome.html     portugueseHTML.txt          "tmp/familyhome.html" >> testall.log


ECHO -=- generating SIMPLIFIED CHINESE web templates  -=-
tokin familyhome.html     SimplifiedChineseHTML.txt   "tmp/familyhome.html" >> testall.log


ECHO -=======- generating SPANISH web templates -=======-
tokin familyhome.html     spanishHTML.txt             "tmp/familyhome.html" >> testall.log


ECHO -=======- generating SWEDISH web templates -=======-
tokin familyhome.html     swedishHTML.txt             "tmp/familyhome.html" >> testall.log


ECHO -========- generating THAI web templates  -========-
tokin familyhome.html     thaiHTML.txt                "tmp/familyhome.html" >> testall.log


ECHO -=- generating TRADITIONAL CHINESE web templates -=-
tokin familyhome.html     TraditionalChineseHTML.txt  "tmp/familyhome.html" >> testall.log
