REM Builds full templates from the set of template data found on source safe...

ECHO Building FRENCH templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\french\0_Sim_Lane"
mkdir "..\french\1_Sim_Lane"
mkdir "..\french\2_Sim_Lane"
mkdir "..\french\3_Sim_Lane"
mkdir "..\french\4_Sim_Lane"
mkdir "..\french\5_Sim_Lane"
mkdir "..\french\6_Sim_Lane"
mkdir "..\french\7_Sim_Lane"
mkdir "..\french\8_Sim_Lane"
mkdir "..\french\9_Sim_Lane"
mkdir "..\french\10_Sim_Lane"

attrib "..\french\0_Sim_Lane\*.*"  -r
attrib "..\french\1_Sim_Lane\*.*"  -r
attrib "..\french\2_Sim_Lane\*.*"  -r
attrib "..\french\3_Sim_Lane\*.*"  -r
attrib "..\french\4_Sim_Lane\*.*"  -r
attrib "..\french\5_Sim_Lane\*.*"  -r
attrib "..\french\6_Sim_Lane\*.*"  -r
attrib "..\french\7_Sim_Lane\*.*"  -r
attrib "..\french\8_Sim_Lane\*.*"  -r
attrib "..\french\9_Sim_Lane\*.*"  -r
attrib "..\french\10_Sim_Lane\*.*" -r
attrib "..\french\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\french\LotTemplates\*.*"   "..\french\0_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\1_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\2_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\3_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\4_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\5_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\6_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\7_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\8_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\9_Sim_Lane"
copy "..\french\LotTemplates\*.*"   "..\french\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\french\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\french\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\french\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\french\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\french\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\french\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\french\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\french\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\french\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\french\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\french\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\french\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\french\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\french\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\french\NeighborhoodGFX"
