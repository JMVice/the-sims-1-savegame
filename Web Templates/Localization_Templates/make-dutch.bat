REM Builds full templates from the set of template data found on source safe...

ECHO Building DUTCH templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\dutch\0_Sim_Lane"
mkdir "..\dutch\1_Sim_Lane"
mkdir "..\dutch\2_Sim_Lane"
mkdir "..\dutch\3_Sim_Lane"
mkdir "..\dutch\4_Sim_Lane"
mkdir "..\dutch\5_Sim_Lane"
mkdir "..\dutch\6_Sim_Lane"
mkdir "..\dutch\7_Sim_Lane"
mkdir "..\dutch\8_Sim_Lane"
mkdir "..\dutch\9_Sim_Lane"
mkdir "..\dutch\10_Sim_Lane"

attrib "..\dutch\0_Sim_Lane\*.*"  -r
attrib "..\dutch\1_Sim_Lane\*.*"  -r
attrib "..\dutch\2_Sim_Lane\*.*"  -r
attrib "..\dutch\3_Sim_Lane\*.*"  -r
attrib "..\dutch\4_Sim_Lane\*.*"  -r
attrib "..\dutch\5_Sim_Lane\*.*"  -r
attrib "..\dutch\6_Sim_Lane\*.*"  -r
attrib "..\dutch\7_Sim_Lane\*.*"  -r
attrib "..\dutch\8_Sim_Lane\*.*"  -r
attrib "..\dutch\9_Sim_Lane\*.*"  -r
attrib "..\dutch\10_Sim_Lane\*.*" -r
attrib "..\dutch\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\dutch\LotTemplates\*.*"   "..\dutch\0_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\1_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\2_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\3_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\4_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\5_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\6_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\7_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\8_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\9_Sim_Lane"
copy "..\dutch\LotTemplates\*.*"   "..\dutch\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\dutch\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\dutch\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\dutch\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\dutch\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\dutch\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\dutch\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\dutch\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\dutch\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\dutch\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\dutch\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\dutch\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\dutch\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\dutch\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\dutch\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\dutch\NeighborhoodGFX"
