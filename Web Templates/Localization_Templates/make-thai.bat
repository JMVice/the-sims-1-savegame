REM Builds full templates from the set of template data found on source safe...

ECHO Building THAI templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\thai\0_Sim_Lane"
mkdir "..\thai\1_Sim_Lane"
mkdir "..\thai\2_Sim_Lane"
mkdir "..\thai\3_Sim_Lane"
mkdir "..\thai\4_Sim_Lane"
mkdir "..\thai\5_Sim_Lane"
mkdir "..\thai\6_Sim_Lane"
mkdir "..\thai\7_Sim_Lane"
mkdir "..\thai\8_Sim_Lane"
mkdir "..\thai\9_Sim_Lane"
mkdir "..\thai\10_Sim_Lane"

attrib "..\thai\0_Sim_Lane\*.*"  -r
attrib "..\thai\1_Sim_Lane\*.*"  -r
attrib "..\thai\2_Sim_Lane\*.*"  -r
attrib "..\thai\3_Sim_Lane\*.*"  -r
attrib "..\thai\4_Sim_Lane\*.*"  -r
attrib "..\thai\5_Sim_Lane\*.*"  -r
attrib "..\thai\6_Sim_Lane\*.*"  -r
attrib "..\thai\7_Sim_Lane\*.*"  -r
attrib "..\thai\8_Sim_Lane\*.*"  -r
attrib "..\thai\9_Sim_Lane\*.*"  -r
attrib "..\thai\10_Sim_Lane\*.*" -r
attrib "..\thai\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\thai\LotTemplates\*.*"   "..\thai\0_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\1_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\2_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\3_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\4_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\5_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\6_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\7_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\8_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\9_Sim_Lane"
copy "..\thai\LotTemplates\*.*"   "..\thai\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\thai\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\thai\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\thai\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\thai\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\thai\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\thai\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\thai\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\thai\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\thai\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\thai\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\thai\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\thai\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\thai\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\thai\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\thai\NeighborhoodGFX"
