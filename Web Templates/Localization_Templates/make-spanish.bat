REM Builds full templates from the set of template data found on source safe...

ECHO Building SPANISH templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\spanish\0_Sim_Lane"
mkdir "..\spanish\1_Sim_Lane"
mkdir "..\spanish\2_Sim_Lane"
mkdir "..\spanish\3_Sim_Lane"
mkdir "..\spanish\4_Sim_Lane"
mkdir "..\spanish\5_Sim_Lane"
mkdir "..\spanish\6_Sim_Lane"
mkdir "..\spanish\7_Sim_Lane"
mkdir "..\spanish\8_Sim_Lane"
mkdir "..\spanish\9_Sim_Lane"
mkdir "..\spanish\10_Sim_Lane"

attrib "..\spanish\0_Sim_Lane\*.*"  -r
attrib "..\spanish\1_Sim_Lane\*.*"  -r
attrib "..\spanish\2_Sim_Lane\*.*"  -r
attrib "..\spanish\3_Sim_Lane\*.*"  -r
attrib "..\spanish\4_Sim_Lane\*.*"  -r
attrib "..\spanish\5_Sim_Lane\*.*"  -r
attrib "..\spanish\6_Sim_Lane\*.*"  -r
attrib "..\spanish\7_Sim_Lane\*.*"  -r
attrib "..\spanish\8_Sim_Lane\*.*"  -r
attrib "..\spanish\9_Sim_Lane\*.*"  -r
attrib "..\spanish\10_Sim_Lane\*.*" -r
attrib "..\spanish\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\spanish\LotTemplates\*.*"   "..\spanish\0_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\1_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\2_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\3_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\4_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\5_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\6_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\7_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\8_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\9_Sim_Lane"
copy "..\spanish\LotTemplates\*.*"   "..\spanish\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\spanish\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\spanish\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\spanish\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\spanish\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\spanish\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\spanish\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\spanish\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\spanish\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\spanish\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\spanish\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\spanish\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\spanish\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\spanish\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\spanish\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\spanish\NeighborhoodGFX"
