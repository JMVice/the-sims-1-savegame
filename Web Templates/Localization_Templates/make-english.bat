REM Builds full templates from the set of template data found on source safe...

ECHO Building ENGLISH templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\english\0_Sim_Lane"
mkdir "..\english\1_Sim_Lane"
mkdir "..\english\2_Sim_Lane"
mkdir "..\english\3_Sim_Lane"
mkdir "..\english\4_Sim_Lane"
mkdir "..\english\5_Sim_Lane"
mkdir "..\english\6_Sim_Lane"
mkdir "..\english\7_Sim_Lane"
mkdir "..\english\8_Sim_Lane"
mkdir "..\english\9_Sim_Lane"
mkdir "..\english\10_Sim_Lane"

attrib "..\english\0_Sim_Lane\*.*"  -r
attrib "..\english\1_Sim_Lane\*.*"  -r
attrib "..\english\2_Sim_Lane\*.*"  -r
attrib "..\english\3_Sim_Lane\*.*"  -r
attrib "..\english\4_Sim_Lane\*.*"  -r
attrib "..\english\5_Sim_Lane\*.*"  -r
attrib "..\english\6_Sim_Lane\*.*"  -r
attrib "..\english\7_Sim_Lane\*.*"  -r
attrib "..\english\8_Sim_Lane\*.*"  -r
attrib "..\english\9_Sim_Lane\*.*"  -r
attrib "..\english\10_Sim_Lane\*.*" -r
attrib "..\english\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\english\LotTemplates\*.*"   "..\english\0_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\1_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\2_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\3_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\4_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\5_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\6_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\7_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\8_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\9_Sim_Lane"
copy "..\english\LotTemplates\*.*"   "..\english\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\english\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\english\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\english\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\english\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\english\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\english\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\english\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\english\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\english\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\english\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\english\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\english\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\english\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\english\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\english\NeighborhoodGFX"
