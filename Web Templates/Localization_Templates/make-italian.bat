REM Builds full templates from the set of template data found on source safe...

ECHO Building ITALIAN templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\italian\0_Sim_Lane"
mkdir "..\italian\1_Sim_Lane"
mkdir "..\italian\2_Sim_Lane"
mkdir "..\italian\3_Sim_Lane"
mkdir "..\italian\4_Sim_Lane"
mkdir "..\italian\5_Sim_Lane"
mkdir "..\italian\6_Sim_Lane"
mkdir "..\italian\7_Sim_Lane"
mkdir "..\italian\8_Sim_Lane"
mkdir "..\italian\9_Sim_Lane"
mkdir "..\italian\10_Sim_Lane"

attrib "..\italian\0_Sim_Lane\*.*"  -r
attrib "..\italian\1_Sim_Lane\*.*"  -r
attrib "..\italian\2_Sim_Lane\*.*"  -r
attrib "..\italian\3_Sim_Lane\*.*"  -r
attrib "..\italian\4_Sim_Lane\*.*"  -r
attrib "..\italian\5_Sim_Lane\*.*"  -r
attrib "..\italian\6_Sim_Lane\*.*"  -r
attrib "..\italian\7_Sim_Lane\*.*"  -r
attrib "..\italian\8_Sim_Lane\*.*"  -r
attrib "..\italian\9_Sim_Lane\*.*"  -r
attrib "..\italian\10_Sim_Lane\*.*" -r
attrib "..\italian\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\italian\LotTemplates\*.*"   "..\italian\0_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\1_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\2_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\3_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\4_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\5_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\6_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\7_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\8_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\9_Sim_Lane"
copy "..\italian\LotTemplates\*.*"   "..\italian\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\italian\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\italian\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\italian\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\italian\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\italian\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\italian\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\italian\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\italian\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\italian\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\italian\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\italian\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\italian\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\italian\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\italian\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\italian\NeighborhoodGFX"
