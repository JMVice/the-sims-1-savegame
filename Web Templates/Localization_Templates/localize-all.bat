REM -=- Generates all translated HTML web templates -=-

del makeall.log

ECHO -========- generating DUTCH web templates -========-
attrib "../dutch/LotTemplates/*.*" -r
attrib "../dutch/*.*" -r
tokin familyhome.html     dutchHTML.txt               "../dutch/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          dutchHTML.txt               "../dutch/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  dutchHTML.txt               "../dutch/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     dutchHTML.txt               "../dutch/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   dutchHTML.txt               "../dutch/neighborhood.html"			>> makeall.log
tokin main.html           dutchHTML.txt               "../dutch/main.html"				>> makeall.log
tokin familyX.html        dutchHTML.txt               "../dutch/familyX.html"				>> makeall.log
tokin navigation.html     dutchHTML.txt               "../dutch/navigation.html"			>> makeall.log
tokin loading.html        dutchHTML.txt               "../dutch/loading.html"				>> makeall.log
tokin publish.html        dutchHTML.txt               "../dutch/publish.html"				>> makeall.log
tokin blank.html          dutchHTML.txt               "../dutch/blank.html"				>> makeall.log
tokin addressbook.html    dutchHTML.txt               "../dutch/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../dutch/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../dutch/LotTemplates/logo2.jpg"
copy thesims.css	"../dutch/thesims.css"
copy sblogo.jpg   	"../dutch/logo.jpg"


ECHO -=======- generating ENGLISH web templates -=======-
attrib "../english/LotTemplates/*.*" -r
attrib "../english/*.*" -r
tokin familyhome.html     englishHTML.txt             "../english/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          englishHTML.txt             "../english/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  englishHTML.txt             "../english/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     englishHTML.txt             "../english/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   englishHTML.txt             "../english/neighborhood.html"			>> makeall.log
tokin main.html           englishHTML.txt             "../english/main.html"				>> makeall.log
tokin familyX.html        englishHTML.txt             "../english/familyX.html"				>> makeall.log
tokin navigation.html     englishHTML.txt             "../english/navigation.html"			>> makeall.log
tokin loading.html        englishHTML.txt             "../english/loading.html"				>> makeall.log
tokin publish.html        englishHTML.txt             "../english/publish.html"				>> makeall.log
tokin blank.html          englishHTML.txt             "../english/blank.html"				>> makeall.log
tokin addressbook.html    englishHTML.txt             "../english/addressbook.html"			>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../english/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../english/LotTemplates/logo2.jpg"
copy thesims.css	"../english/thesims.css"
copy sblogo.jpg   	"../english/logo.jpg"


ECHO -=====- generating ENGLISH UK web templates  -=====-
attrib "../english uk/LotTemplates/*.*" -r
attrib "../english uk/*.*" -r
tokin familyhome.html     englishukHTML.txt           "../english uk/LotTemplates/familyhome.html"	>> makeall.log
tokin house.html          englishukHTML.txt           "../english uk/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  englishukHTML.txt           "../english uk/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     englishukHTML.txt           "../english uk/LotTemplates/scrapbookX.html"	>> makeall.log

tokin neighborhood.html   englishukHTML.txt           "../english uk/neighborhood.html"			>> makeall.log
tokin main.html           englishukHTML.txt           "../english uk/main.html"				>> makeall.log
tokin familyX.html        englishukHTML.txt           "../english uk/familyX.html"			>> makeall.log
tokin navigation.html     englishukHTML.txt           "../english uk/navigation.html"			>> makeall.log
tokin loading.html        englishukHTML.txt           "../english uk/loading.html"			>> makeall.log
tokin publish.html        englishukHTML.txt           "../english uk/publish.html"			>> makeall.log
tokin blank.html          englishukHTML.txt           "../english uk/blank.html"			>> makeall.log
tokin addressbook.html    englishukHTML.txt           "../english uk/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../english uk/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../english uk/LotTemplates/logo2.jpg"
copy thesims.css	"../english uk/thesims.css"
copy sblogo.jpg   	"../english uk/logo.jpg"


ECHO -=======- generating FRENCH web templates  -=======-
attrib "../french/LotTemplates/*.*" -r
attrib "../french/*.*" -r
tokin familyhome.html     frenchHTML.txt              "../french/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          frenchHTML.txt              "../french/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  frenchHTML.txt              "../french/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     frenchHTML.txt              "../french/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   frenchHTML.txt               "../french/neighborhood.html"			>> makeall.log
tokin main.html           frenchHTML.txt               "../french/main.html"				>> makeall.log
tokin familyX.html        frenchHTML.txt               "../french/familyX.html"				>> makeall.log
tokin navigation.html     frenchHTML.txt               "../french/navigation.html"			>> makeall.log
tokin loading.html        frenchHTML.txt               "../french/loading.html"				>> makeall.log
tokin publish.html        frenchHTML.txt               "../french/publish.html"				>> makeall.log
tokin blank.html          frenchHTML.txt               "../french/blank.html"				>> makeall.log
tokin addressbook.html    frenchHTML.txt               "../french/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo_french.jpg		"../french/LotTemplates/logo.jpg"
copy sblogosbk_french.jpg	"../french/LotTemplates/logo2.jpg"
copy thesims.css			"../french/thesims.css"
copy sblogo_french.jpg   	"../french/logo.jpg"


ECHO -=======- generating GERMAN web templates  -=======-
attrib "../german/LotTemplates/*.*" -r
attrib "../german/*.*" -r
tokin familyhome.html     germanHTML.txt              "../german/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          germanHTML.txt              "../german/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  germanHTML.txt              "../german/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     germanHTML.txt              "../german/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   germanHTML.txt               "../german/neighborhood.html"			>> makeall.log
tokin main.html           germanHTML.txt               "../german/main.html"				>> makeall.log
tokin familyX.html        germanHTML.txt               "../german/familyX.html"				>> makeall.log
tokin navigation.html     germanHTML.txt               "../german/navigation.html"			>> makeall.log
tokin loading.html        germanHTML.txt               "../german/loading.html"				>> makeall.log
tokin publish.html        germanHTML.txt               "../german/publish.html"				>> makeall.log
tokin blank.html          germanHTML.txt               "../german/blank.html"				>> makeall.log
tokin addressbook.html    germanHTML.txt               "../german/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo_german.jpg		"../german/LotTemplates/logo.jpg"
copy sblogosbk_german.jpg	"../german/LotTemplates/logo2.jpg"
copy thesims.css			"../german/thesims.css"
copy sblogo_german.jpg		"../german/logo.jpg"


ECHO -=======- generating ITALIAN web templates -=======-
attrib "../italian/LotTemplates/*.*" -r
attrib "../italian/*.*" -r
tokin familyhome.html     italianHTML.txt             "../italian/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          italianHTML.txt             "../italian/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  italianHTML.txt             "../italian/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     italianHTML.txt             "../italian/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   italianHTML.txt               "../italian/neighborhood.html"			>> makeall.log
tokin main.html           italianHTML.txt               "../italian/main.html"				>> makeall.log
tokin familyX.html        italianHTML.txt               "../italian/familyX.html"			>> makeall.log
tokin navigation.html     italianHTML.txt               "../italian/navigation.html"			>> makeall.log
tokin loading.html        italianHTML.txt               "../italian/loading.html"			>> makeall.log
tokin publish.html        italianHTML.txt               "../italian/publish.html"			>> makeall.log
tokin blank.html          italianHTML.txt               "../italian/blank.html"				>> makeall.log
tokin addressbook.html    italianHTML.txt               "../italian/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../italian/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../italian/LotTemplates/logo2.jpg"
copy thesims.css	"../italian/thesims.css"
copy sblogo.jpg   	"../italian/logo.jpg"


ECHO -======- generating JAPANESE web templates  -======-
attrib "../japanese/LotTemplates/*.*" -r
attrib "../japanese/*.*" -r
tokin familyhome.html     japaneseHTML.txt            "../japanese/LotTemplates/familyhome.html"	>> makeall.log
tokin house.html          japaneseHTML.txt            "../japanese/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  japaneseHTML.txt            "../japanese/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     japaneseHTML.txt            "../japanese/LotTemplates/scrapbookX.html"	>> makeall.log

tokin neighborhood.html   japaneseHTML.txt               "../japanese/neighborhood.html"		>> makeall.log
tokin main.html           japaneseHTML.txt               "../japanese/main.html"			>> makeall.log
tokin familyX.html        japaneseHTML.txt               "../japanese/familyX.html"			>> makeall.log
tokin navigation.html     japaneseHTML.txt               "../japanese/navigation.html"			>> makeall.log
tokin loading.html        japaneseHTML.txt               "../japanese/loading.html"			>> makeall.log
tokin publish.html        japaneseHTML.txt               "../japanese/publish.html"			>> makeall.log
tokin blank.html          japaneseHTML.txt               "../japanese/blank.html"			>> makeall.log
tokin addressbook.html    japaneseHTML.txt               "../japanese/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo_simpeople.jpg		"../japanese/LotTemplates/logo.jpg"
copy sblogosbk_simpeople.jpg	"../japanese/LotTemplates/logo2.jpg"
copy thesims.css				"../japanese/thesims.css"
copy sblogo_simpeople.jpg		"../japanese/logo.jpg"


ECHO -=======- generating KOREAN web templates  -=======-
attrib "../korean/LotTemplates/*.*" -r
attrib "../korean/*.*" -r
tokin familyhome.html     koreanHTML.txt              "../korean/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          koreanHTML.txt              "../korean/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  koreanHTML.txt              "../korean/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     koreanHTML.txt              "../korean/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   koreanHTML.txt               "../korean/neighborhood.html"			>> makeall.log
tokin main.html           koreanHTML.txt               "../korean/main.html"				>> makeall.log
tokin familyX.html        koreanHTML.txt               "../korean/familyX.html"				>> makeall.log
tokin navigation.html     koreanHTML.txt               "../korean/navigation.html"			>> makeall.log
tokin loading.html        koreanHTML.txt               "../korean/loading.html"				>> makeall.log
tokin publish.html        koreanHTML.txt               "../korean/publish.html"				>> makeall.log
tokin blank.html          koreanHTML.txt               "../korean/blank.html"				>> makeall.log
tokin addressbook.html    koreanHTML.txt               "../korean/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../korean/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../korean/LotTemplates/logo2.jpg"
copy thesims.css	"../korean/thesims.css"
copy sblogo.jpg   	"../korean/logo.jpg"


ECHO -=======- generating POLISH web templates  -=======-
attrib "../polish/LotTemplates/*.*" -r
attrib "../polish/*.*" -r
tokin familyhome.html     polishHTML.txt              "../polish/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          polishHTML.txt              "../polish/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  polishHTML.txt              "../polish/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     polishHTML.txt              "../polish/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   polishHTML.txt               "../polish/neighborhood.html"			>> makeall.log
tokin main.html           polishHTML.txt               "../polish/main.html"				>> makeall.log
tokin familyX.html        polishHTML.txt               "../polish/familyX.html"				>> makeall.log
tokin navigation.html     polishHTML.txt               "../polish/navigation.html"			>> makeall.log
tokin loading.html        polishHTML.txt               "../polish/loading.html"				>> makeall.log
tokin publish.html        polishHTML.txt               "../polish/publish.html"				>> makeall.log
tokin blank.html          polishHTML.txt               "../polish/blank.html"				>> makeall.log
tokin addressbook.html    polishHTML.txt               "../polish/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../polish/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../polish/LotTemplates/logo2.jpg"
copy thesims.css	"../polish/thesims.css"
copy sblogo.jpg   	"../polish/logo.jpg"


ECHO -=====- generating PORTUGUESE web templates  -=====-
attrib "../portuguese/LotTemplates/*.*" -r
attrib "../portuguese/*.*" -r
tokin familyhome.html     portugueseHTML.txt          "../portuguese/LotTemplates/familyhome.html"	>> makeall.log
tokin house.html          portugueseHTML.txt          "../portuguese/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  portugueseHTML.txt          "../portuguese/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     portugueseHTML.txt          "../portuguese/LotTemplates/scrapbookX.html"	>> makeall.log

tokin neighborhood.html   portugueseHTML.txt               "../portuguese/neighborhood.html"		>> makeall.log
tokin main.html           portugueseHTML.txt               "../portuguese/main.html"			>> makeall.log
tokin familyX.html        portugueseHTML.txt               "../portuguese/familyX.html"			>> makeall.log
tokin navigation.html     portugueseHTML.txt               "../portuguese/navigation.html"		>> makeall.log
tokin loading.html        portugueseHTML.txt               "../portuguese/loading.html"			>> makeall.log
tokin publish.html        portugueseHTML.txt               "../portuguese/publish.html"			>> makeall.log
tokin blank.html          portugueseHTML.txt               "../portuguese/blank.html"			>> makeall.log
tokin addressbook.html    portugueseHTML.txt               "../portuguese/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../portuguese/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../portuguese/LotTemplates/logo2.jpg"
copy thesims.css	"../portuguese/thesims.css"
copy sblogo.jpg   	"../portuguese/logo.jpg"


ECHO -=- generating SIMPLIFIED CHINESE web templates  -=-
attrib "../simplified chinese/LotTemplates/*.*" -r
attrib "../simplified chinese/*.*" -r
tokin familyhome.html     SimplifiedChineseHTML.txt   "../Simplified Chinese/LotTemplates/familyhome.html"	>> makeall.log
tokin house.html          SimplifiedChineseHTML.txt   "../Simplified Chinese/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  SimplifiedChineseHTML.txt   "../Simplified Chinese/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     SimplifiedChineseHTML.txt   "../Simplified Chinese/LotTemplates/scrapbookX.html"	>> makeall.log

tokin neighborhood.html   SimplifiedChineseHTML.txt   "../Simplified Chinese/neighborhood.html"		>> makeall.log
tokin main.html           SimplifiedChineseHTML.txt   "../Simplified Chinese/main.html"			>> makeall.log
tokin familyX.html        SimplifiedChineseHTML.txt   "../Simplified Chinese/familyX.html"		>> makeall.log
tokin navigation.html     SimplifiedChineseHTML.txt   "../Simplified Chinese/navigation.html"		>> makeall.log
tokin loading.html        SimplifiedChineseHTML.txt   "../Simplified Chinese/loading.html"		>> makeall.log
tokin publish.html        SimplifiedChineseHTML.txt   "../Simplified Chinese/publish.html"		>> makeall.log
tokin blank.html          SimplifiedChineseHTML.txt   "../Simplified Chinese/blank.html"		>> makeall.log
tokin addressbook.html    SimplifiedChineseHTML.txt   "../Simplified Chinese/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../simplified chinese/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../simplified chinese/LotTemplates/logo2.jpg"
copy thesims.css	"../simplified chinese/thesims.css"
copy sblogo.jpg   	"../simplified chinese/logo.jpg"


ECHO -=======- generating SPANISH web templates -=======-
attrib "../spanish/LotTemplates/*.*" -r
attrib "../spanish/*.*" -r
tokin familyhome.html     spanishHTML.txt             "../spanish/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          spanishHTML.txt             "../spanish/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  spanishHTML.txt             "../spanish/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     spanishHTML.txt             "../spanish/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   spanishHTML.txt               "../spanish/neighborhood.html"			>> makeall.log
tokin main.html           spanishHTML.txt               "../spanish/main.html"				>> makeall.log
tokin familyX.html        spanishHTML.txt               "../spanish/familyX.html"			>> makeall.log
tokin navigation.html     spanishHTML.txt               "../spanish/navigation.html"			>> makeall.log
tokin loading.html        spanishHTML.txt               "../spanish/loading.html"			>> makeall.log
tokin publish.html        spanishHTML.txt               "../spanish/publish.html"			>> makeall.log
tokin blank.html          spanishHTML.txt               "../spanish/blank.html"				>> makeall.log
tokin addressbook.html    spanishHTML.txt               "../spanish/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo_spanish.jpg		"../spanish/LotTemplates/logo.jpg"
copy sblogosbk_spanish.jpg	"../spanish/LotTemplates/logo2.jpg"
copy thesims.css			"../spanish/thesims.css"
copy sblogo.jpg   			"../spanish/logo.jpg"


ECHO -=======- generating SWEDISH web templates -=======-
attrib "../swedish/LotTemplates/*.*" -r
attrib "../swedish/*.*" -r
tokin familyhome.html     swedishHTML.txt             "../swedish/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          swedishHTML.txt             "../swedish/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  swedishHTML.txt             "../swedish/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     swedishHTML.txt             "../swedish/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   swedishHTML.txt               "../swedish/neighborhood.html"			>> makeall.log
tokin main.html           swedishHTML.txt               "../swedish/main.html"				>> makeall.log
tokin familyX.html        swedishHTML.txt               "../swedish/familyX.html"			>> makeall.log
tokin navigation.html     swedishHTML.txt               "../swedish/navigation.html"			>> makeall.log
tokin loading.html        swedishHTML.txt               "../swedish/loading.html"			>> makeall.log
tokin publish.html        swedishHTML.txt               "../swedish/publish.html"			>> makeall.log
tokin blank.html          swedishHTML.txt               "../swedish/blank.html"				>> makeall.log
tokin addressbook.html    swedishHTML.txt               "../swedish/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../swedish/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../swedish/LotTemplates/logo2.jpg"
copy thesims.css	"../swedish/thesims.css"
copy sblogo.jpg   	"../swedish/logo.jpg"


ECHO -========- generating THAI web templates  -========-
attrib "../thai/LotTemplates/*.*" -r
attrib "../thai/*.*" -r
tokin familyhome.html     thaiHTML.txt                "../thai/LotTemplates/familyhome.html"		>> makeall.log
tokin house.html          thaiHTML.txt                "../thai/LotTemplates/house.html"			>> makeall.log
tokin familymemberX.html  thaiHTML.txt                "../thai/LotTemplates/familymemberX.html"		>> makeall.log
tokin scrapbookX.html     thaiHTML.txt                "../thai/LotTemplates/scrapbookX.html"		>> makeall.log

tokin neighborhood.html   thaiHTML.txt               "../thai/neighborhood.html"			>> makeall.log
tokin main.html           thaiHTML.txt               "../thai/main.html"				>> makeall.log
tokin familyX.html        thaiHTML.txt               "../thai/familyX.html"				>> makeall.log
tokin navigation.html     thaiHTML.txt               "../thai/navigation.html"				>> makeall.log
tokin loading.html        thaiHTML.txt               "../thai/loading.html"				>> makeall.log
tokin publish.html        thaiHTML.txt               "../thai/publish.html"				>> makeall.log
tokin blank.html          thaiHTML.txt               "../thai/blank.html"				>> makeall.log
tokin addressbook.html    thaiHTML.txt               "../thai/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../thai/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../thai/LotTemplates/logo2.jpg"
copy thesims.css	"../thai/thesims.css"
copy sblogo.jpg   	"../thai/logo.jpg"


ECHO -=- generating TRADITIONAL CHINESE web templates -=-
attrib "../traditional chinese/LotTemplates/*.*" -r
attrib "../traditional chinese/*.*" -r
tokin familyhome.html     TraditionalChineseHTML.txt  "../Traditional Chinese/LotTemplates/familyhome.html"	>> makeall.log
tokin house.html          TraditionalChineseHTML.txt  "../Traditional Chinese/LotTemplates/house.html"		>> makeall.log
tokin familymemberX.html  TraditionalChineseHTML.txt  "../Traditional Chinese/LotTemplates/familymemberX.html"	>> makeall.log
tokin scrapbookX.html     TraditionalChineseHTML.txt  "../Traditional Chinese/LotTemplates/scrapbookX.html"	>> makeall.log

tokin neighborhood.html   TraditionalChineseHTML.txt  "../Traditional Chinese/neighborhood.html"	>> makeall.log
tokin main.html           TraditionalChineseHTML.txt  "../Traditional Chinese/main.html"		>> makeall.log
tokin familyX.html        TraditionalChineseHTML.txt  "../Traditional Chinese/familyX.html"		>> makeall.log
tokin navigation.html     TraditionalChineseHTML.txt  "../Traditional Chinese/navigation.html"		>> makeall.log
tokin loading.html        TraditionalChineseHTML.txt  "../Traditional Chinese/loading.html"		>> makeall.log
tokin publish.html        TraditionalChineseHTML.txt  "../Traditional Chinese/publish.html"		>> makeall.log
tokin blank.html          TraditionalChineseHTML.txt  "../Traditional Chinese/blank.html"		>> makeall.log
tokin addressbook.html    TraditionalChineseHTML.txt  "../Traditional Chinese/addressbook.html"				>> makeall.log

REM copy the localized logos and the neighborhood style sheet
copy sblogo.jpg		"../traditional chinese/LotTemplates/logo.jpg"
copy sblogosbk.jpg	"../traditional chinese/LotTemplates/logo2.jpg"
copy thesims.css	"../traditional chinese/thesims.css"
copy sblogo.jpg   	"../traditional chinese/logo.jpg"

REM Always make the english version since I like to keep that up to date in SS
make-english.bat

ECHO -=- Grep makeall.log for 'Token: Couldn't find token' in order to detect errors..... -=-