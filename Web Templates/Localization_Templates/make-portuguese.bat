REM Builds full templates from the set of template data found on source safe...

ECHO Building PORTUGUESE templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\portuguese\0_Sim_Lane"
mkdir "..\portuguese\1_Sim_Lane"
mkdir "..\portuguese\2_Sim_Lane"
mkdir "..\portuguese\3_Sim_Lane"
mkdir "..\portuguese\4_Sim_Lane"
mkdir "..\portuguese\5_Sim_Lane"
mkdir "..\portuguese\6_Sim_Lane"
mkdir "..\portuguese\7_Sim_Lane"
mkdir "..\portuguese\8_Sim_Lane"
mkdir "..\portuguese\9_Sim_Lane"
mkdir "..\portuguese\10_Sim_Lane"

attrib "..\portuguese\0_Sim_Lane\*.*"  -r
attrib "..\portuguese\1_Sim_Lane\*.*"  -r
attrib "..\portuguese\2_Sim_Lane\*.*"  -r
attrib "..\portuguese\3_Sim_Lane\*.*"  -r
attrib "..\portuguese\4_Sim_Lane\*.*"  -r
attrib "..\portuguese\5_Sim_Lane\*.*"  -r
attrib "..\portuguese\6_Sim_Lane\*.*"  -r
attrib "..\portuguese\7_Sim_Lane\*.*"  -r
attrib "..\portuguese\8_Sim_Lane\*.*"  -r
attrib "..\portuguese\9_Sim_Lane\*.*"  -r
attrib "..\portuguese\10_Sim_Lane\*.*" -r
attrib "..\portuguese\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\0_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\1_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\2_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\3_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\4_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\5_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\6_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\7_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\8_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\9_Sim_Lane"
copy "..\portuguese\LotTemplates\*.*"   "..\portuguese\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\portuguese\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\portuguese\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\portuguese\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\portuguese\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\portuguese\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\portuguese\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\portuguese\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\portuguese\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\portuguese\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\portuguese\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\portuguese\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\portuguese\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\portuguese\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\portuguese\NeighborhoodGFX"
