REM Builds full templates from the set of template data found on source safe...

ECHO Building POLISH templates....

ECHO Stage 1 - Make 11 lot directories (0 to 10)
mkdir "..\polish\0_Sim_Lane"
mkdir "..\polish\1_Sim_Lane"
mkdir "..\polish\2_Sim_Lane"
mkdir "..\polish\3_Sim_Lane"
mkdir "..\polish\4_Sim_Lane"
mkdir "..\polish\5_Sim_Lane"
mkdir "..\polish\6_Sim_Lane"
mkdir "..\polish\7_Sim_Lane"
mkdir "..\polish\8_Sim_Lane"
mkdir "..\polish\9_Sim_Lane"
mkdir "..\polish\10_Sim_Lane"

attrib "..\polish\0_Sim_Lane\*.*"  -r
attrib "..\polish\1_Sim_Lane\*.*"  -r
attrib "..\polish\2_Sim_Lane\*.*"  -r
attrib "..\polish\3_Sim_Lane\*.*"  -r
attrib "..\polish\4_Sim_Lane\*.*"  -r
attrib "..\polish\5_Sim_Lane\*.*"  -r
attrib "..\polish\6_Sim_Lane\*.*"  -r
attrib "..\polish\7_Sim_Lane\*.*"  -r
attrib "..\polish\8_Sim_Lane\*.*"  -r
attrib "..\polish\9_Sim_Lane\*.*"  -r
attrib "..\polish\10_Sim_Lane\*.*" -r
attrib "..\polish\NeighborhoodGFX\*.*"  -r

ECHO Stage 2 - Put the main templates and localized logo in each lot directory
copy "..\polish\LotTemplates\*.*"   "..\polish\0_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\1_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\2_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\3_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\4_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\5_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\6_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\7_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\8_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\9_Sim_Lane"
copy "..\polish\LotTemplates\*.*"   "..\polish\10_Sim_Lane"

ECHO Stage 3 - Copy universal lot graphics
copy "..\LotGFX\AllLots\*.*"   "..\polish\0_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\1_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\2_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\3_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\4_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\5_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\6_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\7_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\8_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\9_Sim_Lane"
copy "..\LotGFX\AllLots\*.*"   "..\polish\10_Sim_Lane"

ECHO Stage 4 - Copy lot number variant graphics
copy "..\LotGFX\0_Sim_Lane\*.*"   "..\polish\0_Sim_Lane"
copy "..\LotGFX\1_Sim_Lane\*.*"   "..\polish\1_Sim_Lane"
copy "..\LotGFX\2_Sim_Lane\*.*"   "..\polish\2_Sim_Lane"
copy "..\LotGFX\3_Sim_Lane\*.*"   "..\polish\3_Sim_Lane"
copy "..\LotGFX\4_Sim_Lane\*.*"   "..\polish\4_Sim_Lane"
copy "..\LotGFX\5_Sim_Lane\*.*"   "..\polish\5_Sim_Lane"
copy "..\LotGFX\6_Sim_Lane\*.*"   "..\polish\6_Sim_Lane"
copy "..\LotGFX\7_Sim_Lane\*.*"   "..\polish\7_Sim_Lane"
copy "..\LotGFX\8_Sim_Lane\*.*"   "..\polish\8_Sim_Lane"
copy "..\LotGFX\9_Sim_Lane\*.*"   "..\polish\9_Sim_Lane"
copy "..\LotGFX\10_Sim_Lane\*.*"  "..\polish\10_Sim_Lane"

ECHO Stage 5 - Copy address book graphics
mkdir "..\polish\NeighborhoodGFX"
copy  "..\LotGFX\NeighborhoodGFX\*.*"   "..\polish\NeighborhoodGFX"
